public class Libro
{
    private String isbn;
    private String titulo;
    private String autor; 
    private int numeroPaginas;

    //setters
    public void setTitulo(String titulo){
        this.titulo=titulo;
    }

    public void setAutor(String autor){
        this.autor=autor;
    }

    public void setNumeroPagina(int numeroPaginas){
        this.numeroPaginas=numeroPaginas;
    }

    public void setISBN(String isbn){
        this.isbn=isbn;
    }


    //getter
    public String getTitulo(){
        return titulo;
    }

    public int getNumerPaginas(){
        return numeroPaginas;
    }

    public String getAutor(){
        return autor;
    }

    public String getISBN(){
        return isbn;
    }

    @Override
    public String toString(){
        return "El libro con ISBN " + isbn + ", creado por el autor: " + autor + ", tiene "
         + numeroPaginas + " paginas";
    }
}