public class Principal {
    public static void main (String[] args){
        Libro libro1=new Libro();

        libro1.setISBN("978-950-23-2217-9");
        libro1.setAutor("Andres Juarez");
        libro1.setTitulo("JAVA");
        libro1.setNumeroPagina(287);

        Libro libro2=new Libro();

        libro2.setISBN("978-607481785-0");
        libro2.setAutor("Juan Perez");
        libro2.setTitulo("Matematica Financiera");
        libro2.setNumeroPagina(321);

        System.out.println("Datos del libro1: " + libro1.toString());
        System.out.println("Datos del libro2: " + libro2.toString());
    }   
}